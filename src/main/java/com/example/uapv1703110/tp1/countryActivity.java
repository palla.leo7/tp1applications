package com.example.uapv1703110.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.*;
import android.content.Intent;
import java.net.URI;

public class countryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        Intent intent = getIntent();
        TextView countryView = (TextView) findViewById(R.id.countryName);
        ImageView flagView = (ImageView) findViewById(R.id.imageView);
        TextView capitaleView = (TextView) findViewById(R.id.editCapitale);
        TextView languageView = (TextView) findViewById(R.id.editLanguage);
        TextView currencyView = (TextView) findViewById(R.id.editCurrency);
        TextView popView = (TextView) findViewById(R.id.editPop);
        TextView sizeView = (TextView) findViewById(R.id.editSize);
        flagView.setImageResource(getResources().getIdentifier(CountryList.getCountry(intent.getStringExtra("country")).getmImgFile(), "drawable", getPackageName()));
        countryView.setText(intent.getStringExtra("country"));
        capitaleView.setText(CountryList.getCountry(intent.getStringExtra("country")).getmCapital());
        languageView.setText(CountryList.getCountry(intent.getStringExtra("country")).getmLanguage());
        currencyView.setText(CountryList.getCountry(intent.getStringExtra("country")).getmCurrency());
        popView.setText(Integer.toString(CountryList.getCountry(intent.getStringExtra("country")).getmPopulation()));
        sizeView.setText(Integer.toString(CountryList.getCountry(intent.getStringExtra("country")).getmArea()));
    }

    public void saveInfo(View view)
    {
        TextView capitaleView = (TextView) findViewById(R.id.editCapitale);
        TextView languageView = (TextView) findViewById(R.id.editLanguage);
        TextView currencyView = (TextView) findViewById(R.id.editCurrency);
        TextView popView = (TextView) findViewById(R.id.editPop);
        TextView sizeView = (TextView) findViewById(R.id.editSize);
        Intent intent = getIntent();
        TextView countryView = (TextView) findViewById(R.id.countryName);
        CountryList.getCountry(intent.getStringExtra("country")).setmCapital(capitaleView.getText().toString());
        CountryList.getCountry(intent.getStringExtra("country")).setmLanguage(languageView.getText().toString());
        CountryList.getCountry(intent.getStringExtra("country")).setmCurrency(currencyView.getText().toString());
        CountryList.getCountry(intent.getStringExtra("country")).setmPopulation(Integer.parseInt(popView.getText().toString()));
        CountryList.getCountry(intent.getStringExtra("country")).setmArea(Integer.parseInt(sizeView.getText().toString()));

    }
}
